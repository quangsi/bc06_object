// thêm sv
// npx surge
// --- deploy
var dssv = [];
const DSSV_LOCAL = 'DSSV_LOCAL';
// khi user load trang => lấy dữ liệu từ localStorage

var jsonData = localStorage.getItem(DSSV_LOCAL);
if (jsonData != null) {
  // JSON.parse(jsonData) => array
  // convert array cũ ( lấy localStorage ) => không có key tinhDTB() => khi lưu xuống bị mất => khi lấy lên ko còn => convert thành array mới
  dssv = JSON.parse(jsonData).map(function (item) {
    // item : phần tử của array trong các lần lặp
    // return của map()
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
  });
  console.log(`  🚀: dssv`, dssv);
  renderDSSV(dssv);
  // map js
}
console.log(`  🚀: jsonData`, jsonData);

function themSV() {
  var sv = layThongTinTuForm();
  //   push(): thêm phần tử vào array

  // kt mã
  var isValid =
    kiemTraTrung(sv.ma, dssv) && kiemTraDoDai(sv.ma, 'spanMaSV', 4, 6);
  // kt email
  isValid = isValid & kiemTraEmail(sv.email);
  // kt username
  isValid = isValid & kiemTraChuoi(sv.ten, 'spanTenSV');
  // validate giữa các tiêu chí cho 1 input : &&
  // kết hợp validate giữa các ô input : &
  //
  if (isValid) {
    dssv.push(sv);
    // convert data
    let dataJson = JSON.stringify(dssv);
    // lưu vào localStorage
    localStorage.setItem(DSSV_LOCAL, dataJson);
    //   render dssv lên table
    renderDSSV(dssv);
    //   tbodySinhVien
    resetForm();
  }
}
function xoaSV(id) {
  // splice: cut ,slice: copy
  var viTri = -1;
  for (var i = 0; i < dssv.length; i++) {
    if (dssv[i].ma == id) {
      viTri = i;
    }
  }
  if (viTri != -1) {
    // nếu tìm thấy vị trí thì xoá
    //   splice ( vị trí, số lượng)
    dssv.splice(viTri, 1);
    renderDSSV(dssv);
  }
}

function suaSV(id) {
  console.log(`  🚀: id`, id);
  var viTri = dssv.findIndex(function (item) {
    return item.ma == id;
  });
  console.log(`  🚀: suaSV -> viTri`, viTri);
  // show thông tin lên form
  var sv = dssv[viTri];
  document.getElementById('txtMaSV').value = sv.ma;
  document.getElementById('txtTenSV').value = sv.ten;
  document.getElementById('txtEmail').value = sv.email;
  document.getElementById('txtPass').value = sv.matKhau;
  document.getElementById('txtDiemToan').value = sv.toan;
  document.getElementById('txtDiemLy').value = sv.ly;
  document.getElementById('txtDiemHoa').value = sv.hoa;
}

function capNhatSV() {
  //  layThongTinTuForm() => return object sv
  var sv = layThongTinTuForm();
  console.log(`  🚀: capNhatSV -> sv`, sv);
  var viTri = dssv.findIndex(function (item) {
    return item.ma == sv.ma;
  });
  dssv[viTri] = sv;
  renderDSSV(dssv);
}

function resetForm() {
  document.getElementById('formQLSV').reset();
}
// <!-- splice,findIndex,map,push, forEach -->

// <!-- localStorage : lư trữ , JSON : convert data -->

// var fodds = ['bún riu', 'bún bò'];
// fodds[1] = 'bún cá';

// diable input js

// deploy
