function showMessage(idTag, message) {
  document.getElementById(idTag).innerHTML = message;
}

// hợp lệ => true
function kiemTraTrung(id, dssv) {
  let viTri = dssv.findIndex(function (sv) {
    return sv.ma == id;
  });
  if (viTri != -1) {
    //  tìm thấy
    showMessage('spanMaSV', 'Mã sinh viên đã tồn tại');
    return false;
  } else {
    showMessage('spanMaSV', '');
    return true;
  }
}
function kiemTraEmail(email) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  var isEmail = re.test(email);
  if (isEmail) {
    showMessage('spanEmailSV', '');
    return true;
  } else {
    showMessage('spanEmailSV', 'email không hợp lệ');

    return false;
  }
}
function kiemTraDoDai(value, idErr, min, max) {
  var length = value.length;
  if (length >= min && length <= max) {
    showMessage(idErr, '');
    return true;
  } else {
    showMessage(idErr, `Trường này phải gồm ${min} đến ${max} kí tự`);
    return false;
  }
}

function kiemTraChuoi(value, idErr) {
  var re = /^[a-zA-Z\-]+$/;
  if (re.test(value)) {
    showMessage(idErr, '');
    return true;
  } else {
    showMessage(idErr, 'Trường này chỉ gồm chuỗi');
    return false;
  }
}
