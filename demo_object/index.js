// array [] ~ lưu nhiều thông tin cùng loại ~ danh sách sv, danh sách sdt

// object { } ~ mô tả các thông tin khác nhau của 1 đối tượng ( nhân vật game, nhân viên trong công ty)

// key: value

var sv1 = {
  name: 'alice',
  age: 3,
  gmail: 'alice@gmail.com',
};
console.log(`  🚀: sv1`, sv1);

var sv2 = {
  name: 'bob',
  age: 2,
  gmail: 'bob@gmail.com',
  xinTienHoc: function () {
    console.log('con là ', this.name);
  },
};
// dùng key để lấy value
/**
 * lấy giá trị
 * array => index
 * object => key
 */
console.log('ten sv2', sv2.name);

var cat1 = {
  // property
  name: 'miu',
  age: 1,
  // method
  speak: funQLSV,
};
// update value
cat1.name = 'mun';
console.log(`  🚀: cat1`);
cat1.speak();
cat1.child.speak();
// data type : string, number, boolean, null, undefied, array,object
